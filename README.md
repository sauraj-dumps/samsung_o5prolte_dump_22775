## o5proltedd-user 7.1.1 NMF26X G550FYXXU1CRD1 release-keys
- Manufacturer: samsung
- Platform: 
- Codename: o5prolte
- Brand: samsung
- Flavor: lineage_o5prolte-eng
- Release Version: 10
- Id: QQ3A.200805.001
- Incremental: eng.cirrus.20211212.122424
- Tags: test-keys
- CPU Abilist: armeabi-v7a,armeabi
- A/B Device: false
- Locale: en-US
- Screen Density: undefined
- Fingerprint: samsung/o5proltedd/o5prolte:7.1.1/NMF26X/G550FYXXU1CRD1:user/release-keys
- OTA version: 
- Branch: o5proltedd-user-7.1.1-NMF26X-G550FYXXU1CRD1-release-keys
- Repo: samsung_o5prolte_dump_22775


>Dumped by [Phoenix Firmware Dumper](https://github.com/DroidDumps/phoenix_firmware_dumper)
