#!/system/bin/sh
if ! applypatch --check EMMC:/dev/block/platform/13540000.dwmmc0/by-name/RECOVERY:12648464:187ef5dbb8305723d3d8b5527e05903467a857df; then
  applypatch --bonus /system/etc/recovery-resource.dat \
          --patch /system/recovery-from-boot.p \
          --source EMMC:/dev/block/platform/13540000.dwmmc0/by-name/BOOT:6557712:666d81e0a8234bfa69376da6586a7d5ad3f11788 \
          --target EMMC:/dev/block/platform/13540000.dwmmc0/by-name/RECOVERY:12648464:187ef5dbb8305723d3d8b5527e05903467a857df && \
      log -t recovery "Installing new recovery image: succeeded" || \
      log -t recovery "Installing new recovery image: failed"
else
  log -t recovery "Recovery image already installed"
fi
